import { Component, OnInit,OnChanges } from '@angular/core';
import { LoginService } from '../../core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit,OnChanges {
password:any=""
email:any=""
message={
  email:"",
  password:""
}
epwd=false
maile=false
show=false
isauth=false;
userdata:any;
payment=false;
prog=false;
loginform=true
auth_data:any

  constructor(private loginservice:LoginService) { }
ngOnChanges(){
  
}
  ngOnInit(): void {
   
  }

toggle(){
  this.show=!this.show;
}
  login(){
    if(this.password && this.email){
      if(this.show){
        this.toggle();
      }
      this.toggle()
      this.loginservice.login({email:this.email,password:this.password}).subscribe(
        res=>{
          if(res.status){
            let id=res.user.id;
            this.userdata=res.user.data
            Object.assign(this.userdata,{id:id});
            let now =new Date()
            let expiryDate = new Date(now.getTime() + res.token.exp*1000);
         this.auth_data={
           token:res.token.access_token,
           expiryDate:expiryDate,
           user:this.userdata.name,
           id:this.userdata.user_id
          }
          try{
            localStorage.setItem('access_token',"0");
            localStorage.setItem('user',id);
            localStorage.setItem("token",JSON.stringify(this.auth_data));
            location.href="/home"
          }catch(e:any){
            console.log(e)
          }

          }
          if(res.message!=undefined){
            this.toggle()
            this.maile=!this.maile;
            this.message.email=res.message; 
          }
          
        },
        err=>{
          this.toggle();
          console.log(err);
          if(err.error.text!=undefined){
           this.maile=!this.maile;
           this.message.email=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.epwd=!this.epwd
            this.message.password=err.error.password;   
          }  
        }
      )
    }
  }


  

 


}


