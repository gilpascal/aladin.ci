import { Component, OnInit,OnChanges } from '@angular/core';
import { LoginService } from '../../core';
import { SocialAuthService, SocialUser, GoogleLoginProvider} from "angularx-social-login";
declare var FB: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit,OnChanges {
  public user: SocialUser = new SocialUser;

  
password:any;
email:any;
id:any;
message={
  email:"",
  password:""
}
response: any;
userInfo: any;
spinning=true;
epwd=false
maile=false
show=false
isauth=false;
userdata:any;
payment=false;
prog=false;
loginform=false;
auth_data:any


  constructor(private loginservice:LoginService, private authService: SocialAuthService) { 
    FB.fbAsyncInit = () => {
      FB.init({
        appId: 'your-app-id',
        cookie: true,
        xfbml: true,
        version: 'v5.0'
      });
      FB.AppEvents.logPageView();
    };
  }
ngOnChanges(){
  
}
  ngOnInit(): void {
    
    ((d, s, id) => {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      //js.src = "https://connect.facebook.net/en_US/sdk.js";
      //fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
 
    this.statusChange();

    

    this.authService.authState.subscribe(user => {
      this.user = user;
      console.log(user);
    });

    if(localStorage.getItem('token')){
      let token:any= localStorage.getItem("token");
      token= JSON.parse(token);
      this.id=token.id;
      this.loginservice.getUser(token.id).subscribe(
        res=>{
          let r:any=res
          console.log(r)
          if(r.status){
           
            this.userdata=r.data[0];
            console.log(this.userdata)
            this.isauth=true;
            this.loginform=false;
            this.spinning=!this.spinning
  
          }
        }
      )
     
    }else{
      this.loginform=true;
      this.spinning=!this.spinning

    }
  }
  private statusChange(): void {
    FB.Event.subscribe('auth.statusChange', (response:any) => {
      console.log("submit login to facebook", response);
      if (response.status === 'connected') {
        if (response.authResponse) {
          FB.api('/me', {
            fields: 'last_name, first_name, email , picture , middle_name, name, name_format, short_name',
          }, (userInfo:any) => {
            console.log('userInfo', userInfo);
       // your-code-goes-here
          });
        }
      }
      else {
        console.log('User login failed');
      }
    });
  }

  public signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  public signOut(): void {
    this.authService.signOut();
  }

  toggle(){
    this.show=!this.show;
  }
  login(){
    if(this.password && this.email){
      if(this.show){
        this.toggle();
      }

      this.toggle()
      this.loginservice.login({email:this.email,password:this.password}).subscribe(
        res=>{
          if(res.status){
            let id=res.user.id;
            this.isauth=!this.isauth;
            this.loginform=!this.loginform
            this.userdata=res.user.data
            Object.assign(this.userdata,{id:id});
            let now =new Date()
          let expiryDate = new Date(now.getTime() + res.token.exp*1000);
          this.auth_data={
           token:res.token.access_token,
           expiryDate:expiryDate,
           user:this.userdata.name,
           id:this.userdata.user_id
          }
          try{
           localStorage.setItem('access_token',res.user.is_partner);
           localStorage.setItem('user',res.user.id);
            localStorage.setItem('token',JSON.stringify(this.auth_data));
            location.reload()
          }catch(e:any){
            console.log(e)
          }

          }
          if(res.message!=undefined){
            this.toggle()
            this.maile=!this.maile;
            this.message.email=res.message; 
          }
          
        },
        err=>{
          this.toggle();
          console.log(err);
          if(err.error.text!=undefined){
           this.maile=!this.maile;
           this.message.email=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.epwd=!this.epwd
            this.message.password=err.error.password;   
          }  
        }
      )
    }
  }


  

 


}



