import { Component, Input, OnInit } from '@angular/core';
import { RegisterService } from '../../core';
import { AuthinfoService } from 'src/app/core/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocialAuthService, GoogleLoginProvider, FacebookLoginProvider, SocialUser, SocialAuthServiceConfig } from 'angularx-social-login';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public user: SocialUser = new SocialUser
  public isSignin:boolean | undefined;
info="Bienvenue chez ALADIN";
data={
  fname:"",
  lname:"",
  email:"",
  phone:"",
  password:"",
  cpwd:"",
  is_partner:0,
  city:"",
  whatapp:""

}
errmsg={
  password:false,
  email:false,
  phone:false,
  infoemail:"",
  infophone:"",
  infopwd:""
}
auth_data:any;
show:boolean=false;
ishttpLoaded:boolean=false;
isLoaded:boolean=false;

  constructor(private registerservice: RegisterService,private l:AuthinfoService, private formBuilder: FormBuilder, private SocialAuthService:SocialAuthService, private authService: SocialAuthService) { }

  ngOnInit(): void {
    this.authService.authState.subscribe(user => {
      this.user = user;
      this.isSignin = (user != null);
      console.log(user);
    });
  }
   
  public signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  public signOut(): void {
    this.authService.signOut();
  }

  toggleSpinner(){
    this.show = !this.show;

  }

  register(event:Event){
    if(this.data.fname!=null&&this.data.lname!=null&&this.data.phone!=null&&this.data.email){
      if(this.matchpwd(this.data.password,this.data.cpwd)==false){
        this.errmsg.password=!this.errmsg.password;
        this.errmsg.infopwd="passwords do not macth";
      }else{

        if(this.show){
          this.toggleSpinner();
        }

        this.toggleSpinner();
        this.registerservice.saveUser({name:this.data.fname + " "+ this.data.lname,email:this.data.email,phone:this.data.phone,password:this.data.password,is_partner:this.data.is_partner,city:this.data.city,whatsapp:this.data.whatapp}).subscribe(res=>{
         console.log(res);
         if(res.data!=undefined){
          let now =new Date()  
          let expiryDate = new Date(now.getTime() + res.token.exp*1000);
          this.auth_data={
            token:res.token.access_token,
            expiryDate:expiryDate,
            user:this.data.fname + " "+ this.data.lname,
            id:res.data.insertId
           } 
      
          try{
            this.l.setItem('access_token',"0");
            this.l.setItem('user',res.data.insertId);
            localStorage.setItem("token",JSON.stringify(this.auth_data))
            location.href='/home';

          }catch(e:any){
             console.log(e)
           }
         }else{
          this.toggleSpinner()
          this.errmsg.email=!this.errmsg.email;
          this.errmsg.infoemail="email existe déjà"
         }

         /**
          * 
         
          */
        },
        err=>{
          this.toggleSpinner();
          console.log(err)
          if(err.error.phone!=undefined){
            this.errmsg.phone=!this.errmsg.phone;
            this.errmsg.infophone=err.error.phone;
       
          }

          if(err.error.password!=undefined){
            this.errmsg.password=!this.errmsg.password;
            this.errmsg.infopwd=err.error.password;   
          }

          if(err.error.email!=undefined){
            this.errmsg.email=!this.errmsg.email;
            this.errmsg.infoemail=err.error.email;
             
          }
        }); 
      
    } 
} 
}

matchpwd(pwd:string,cpwd:string):boolean{
    if(pwd===cpwd){
      return true;
    }else{
      return false;
    }
}

}
