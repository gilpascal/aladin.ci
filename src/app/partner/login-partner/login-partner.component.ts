import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/core';
@Component({
  selector: 'app-login-partner',
  templateUrl: './login-partner.component.html',
  styleUrls: ['./login-partner.component.scss']
})
export class LoginPartnerComponent implements OnInit {
user={
  email:"",
  password:""
}
show=false;
message={
  email:"",
  password:"",
  errpwd:false,
  erremail:false
}
  constructor(private login:LoginService) { }
 
  ngOnInit(): void {
  }

  toggleSpinner(){
    this.show = !this.show;
  }
 Login(){
   if(this.user.email!=null&&this.user.password!=null){
    this.toggleSpinner()
     this.login.login({email:this.user.email,password:this.user.password}).subscribe(
       resp=>{
         console.log(resp)
         localStorage.setItem('access_token',resp.user.is_partner);
         JSON.stringify(localStorage.setItem('part',resp.user_id));
         window.location.href="/partners/dashboard";
       },
       err=>{
        this.toggleSpinner()
         console.log(err)
         if(err.error.password){
           this.message.errpwd=!this.message.errpwd;
           this.message.password=err.error.password;
         
         }

         if(err.error.email){
          this.message.erremail=!this.message.erremail;
          this.message.email=err.error.email;
        
        }
       }
     )
   }
 }


}
