import { Component, Input, OnInit,OnChanges } from '@angular/core';
import { fabric } from 'fabric';
declare var require: any
declare var FontFace:any;
var $ = require("jquery");
import { ColorEvent } from 'ngx-color';
var myalert=require('sweetalert2')
var domtoimage=require('dom-to-image');

import { LocalService,AladinService } from 'src/app/core';

import { ObservableserviceService } from 'src/app/core/rxjs/observableservice.service';

@Component({
  selector: 'app-aladin-models',
  templateUrl: './aladin-models.component.html',
  styleUrls: ['./aladin-models.component.scss']
})
export class AladinModelsComponent implements OnInit,OnChanges {
@Input() height:any=600;
@Input()width:any=600;
@Input() canvas:any;
mysize:any=0;
change=false
innercanvas:any

@Input() url:any;
  constructor(private aladin:AladinService) { }

  ngOnInit(): void {
    this.canvas  = new fabric.Canvas('artcanvas');
    this.innercanvas=new fabric.Canvas("innerCanvas");
    //this.aladin.makeImage(this.url,this.canvas)
    this.aladin.addText(this.innercanvas,this.url)


  }

ngOnChanges(){
  console.log(this.url)
}
  updateTshirtImage(imageURL:any){
    // If the user doesn't pick an option of the select, clear the canvas
    if(!imageURL){
        this.canvas.clear();
    }

    // Create a new image that can be used in Fabric with the URL
    fabric.Image.fromURL(imageURL, (img:any)=> {
        // Define the image as background image of the Canvas
        this.canvas.setBackgroundImage(img, this.canvas.renderAll.bind(this.canvas), {
            // Scale the image to the canvas size
            scaleX: this.canvas.width / img.width,
            scaleY: this.canvas.height /img.height
        });
    });
}


 upload= async ()=>{
  var node :any= document.getElementById('tshirt-div');
domtoimage.toPng(node).then(function (dataUrl:any) {
    // Print the data URL of the picture in the Console
    console.log(dataUrl);

    // You can for example to test, add the image at the end of the document
    var img = new Image();
    img.src = dataUrl;
   // img.
    document.body.appendChild(img);
}).catch(function (error:any) {
    console.error('oops, something went wrong!', error);
});
}


dragEvent(event:any){
  event.dataTransfer.setData("text/html", event.target.outerHTML);
  console.log(event)

}

OnInputChange=(event:any)=> {
  console.log("executed");
var c:any= document.getElementById("canvas-body");
var canvas=document.createElement("canvas")
canvas.width= parseInt(this.mysize);
canvas.height=parseInt(this.mysize);

setTimeout(()=>{
  this.canvas = new fabric.Canvas('models',{ 
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking:false,
    stateful:true
  }); 
  this.aladin.addText(this.canvas);
},2000)


}




}
