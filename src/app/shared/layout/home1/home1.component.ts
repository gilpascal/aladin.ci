import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.scss']
})
export class Home1Component implements OnInit {
   tops:any;
   url='/editor/tops/'
  constructor(private l : ListService) { }

  ngOnInit(): void {
    this.l.gettops().subscribe(res=>{this.tops=res},err=>{console.log(err)});
  }

}
