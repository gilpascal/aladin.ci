import { Component, OnInit,Input } from '@angular/core';
import { AuthinfoService } from 'src/app/core';
@Component({
  selector: 'app-header-up',
  templateUrl: './header-up.component.html',
  styleUrls: ['./header-up.component.scss']
})
export class HeaderUPComponent implements OnInit {

  @Input() id :any
  url="/users/"
  cart_items=0
  constructor(private auth : AuthinfoService) { }

  ngOnInit(): void {
    if(localStorage.getItem('cart')){
      let item :any=localStorage.getItem('cart');
      item=JSON.parse(item);
      this.cart_items=item.length;

    }
  }
logout(event:Event)

{
  try{
    this.auth.removeItem('access_token');
    this.auth.removeItem('token');
    this.auth.removeItem('user');
    window.location.href='/';

  }catch(e:any){
    console.log(e)
  }
  


 
}


}
