import { Component, OnInit,Input, asNativeElements } from '@angular/core';
import { LoginService,FormvalidationService ,AuthinfoService,LocalService,ListService,CartService} from 'src/app/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocialAuthService, GoogleLoginProvider,FacebookLoginProvider, SocialUser } from 'angularx-social-login';

declare var require: any
var $ = require("jquery");
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  loginForm: FormGroup | undefined;
  socialUser: SocialUser = new SocialUser;
  isLoggedin: boolean = false;
  Loggedin: boolean = false;
 

  
  data={
   email:"",
   password:"",
   erroremail:false,
   errorpwd:false,
   pwdmsg:"",
   emailmsg:""
  }
 URL="/users/"
 message="";
 user:any;
 name:any;
 ID:any
 @Input() cart_items:number=0;
 isauth=false;
 show:boolean=false;
 auth_data:any;
 @Input() fromhome=false;
  constructor(private loginservice:LoginService,private valideform:FormvalidationService,private route:Router,private authuser:AuthinfoService,private ls :LocalService,private triger:ListService,private cartInfo: CartService, private socialAuthService: SocialAuthService,private authService: SocialAuthService,private formBuilder: FormBuilder) {console.log(this.isLoggedin) }

  ngOnInit(): void {
         
    /*this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });    
    
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      this.isLoggedin = (user != null);
      this.Loggedin = (user != null);
      console.log(this.socialUser);
    });*/
    

   this.plus();
    this.cart_items=this.ls.cart_items.length;
    if(localStorage.getItem("token")){
      let token:any=localStorage.getItem('token'); 
      this.name=JSON.parse(token).user
      this.isauth=true;
      this.ID=JSON.parse(token).id;
     let now = new Date()

      if(now<token.expiryDate){
        this.user.name= token.user
        this.isauth=true;
        console.log(token.user,"ok")

      }else{
        console.log(token)
      }
     
    }

  }

/**
 * GOOGLE AUTHENTICATION
 */
  loginWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((user:any)=>{
      console.log(user)
     this.loginservice.checkMail(user.email).subscribe(res=>{
       console.log(res)
       try{
        /*this.toggleSpinner();
        let id= document.getElementById("c");
        this.triger.triggerMouse(id)
        this.authuser.setItem('access_token',res.user.is_partner);
        this.authuser.setItem('user',res.user.id);
        localStorage.setItem('token',JSON.stringify(this.auth_data));
        */
        
      }catch(error){
        
      }
     })
     //console.log(this.loginservice.checkMail(user.email))
    }) ;
    
  }

  
  logOut(): void {
    this.socialAuthService.signOut();
  }

/**
 * FACEBOOK AUTHENTICATION
 */
  loginWithFacebook(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.socialAuthService.signOut();
  }


  toggleSpinner(){
    this.show = !this.show;

  }


  plus(){
    this.cartInfo.cartUpdated.subscribe(
      cart_length=>{
        if(cart_length>0){
          this.cart_items=cart_length
          console.log(cart_length,"ok")
        }else{
          console.log(cart_length,"err")
          this.cart_items=cart_length
        }
      }
    )
  }

  login(event:Event){

    if(this.valideform.validateEmail(this.data.email)){
      if(this.data.password!=null){
        if(this.show){
          this.toggleSpinner()
        }
        this.toggleSpinner();
        this.loginservice.login(this.data).subscribe(res=>{
         console.log(res);
         if(res.user!=undefined){    
          //this.isauth=!this.isauth;
          this.user=res.user.data;
          this.name=this.user.name;
          this.ID=this.user.user_id;
          var now = new Date();
          this.isauth=true;
          let expiryDate = new Date(now.getTime() + res.token.exp*1000);
         this.auth_data={
           token:res.token.access_token,
           expiryDate:expiryDate,
           user:this.user.name,
           id:this.user.user_id
          }
          try{
            this.toggleSpinner();
            let id= document.getElementById("c");
            this.triger.triggerMouse(id)
            this.authuser.setItem('access_token',res.user.is_partner);
            this.authuser.setItem('user',res.user.id);
            localStorage.setItem('token',JSON.stringify(this.auth_data));
            

          }catch(err){
            console.log(err)
          }

         
         }
         this.toggleSpinner();
         this.data.erroremail=!this.data.erroremail;
         this.message=res.message;   
        
        },err=>{
          this.toggleSpinner();
           if(err.error.text!=undefined){
           this.data.erroremail=!this.data.erroremail;
           this.data.emailmsg=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.data.errorpwd=!this.data.errorpwd;
            this.data.pwdmsg=err.error.password;   
          }
          if(this.data.erroremail!=true&&this.data.errorpwd!=true){
          console.log("Une erreur  s'est produite veuillez contactez le service client au 27 23 45 73 02")
          }   
        });
      }
    }else{
      this.data.emailmsg="invalide email";
      this.data.erroremail=!this.data.erroremail;

    }

  }

  logout(event:any){
     localStorage.removeItem('token');
     localStorage.removeItem('user');
     localStorage.removeItem('access_token')


     this.isauth=false;
     this.toggleSpinner();
     window.location.reload()
  }
 Scroll(){
   let view=document.getElementById('sommes')
   view?.scrollIntoView({behavior:'smooth'})
 }


 gotocart(){
  this.ls.activatecart.next(true);

 }


 gohome(){
   this.ls.activatecart.next(false);
 }
}
